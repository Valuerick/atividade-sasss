<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Atividade Proposta de SASS</title>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">

</head>
<body>

        <script src="js/jquery.min.js"></script>
    	<script src="js/popper.min.js"></script>
    	<script src="js/bootstrap.min.js"></script>
        
    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <nav class="menuTopo">
                    <ul>
                    
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Empresa</a></li>
                        <li><a href="#">Produtos</a></li>
                        <li><a href="#">Serviços</a></li>
                        <li><a href="#">Contato</a></li>

                    </ul>
                </nav>
            </div>
        </div>

        <div class="row">
            <div class="col text-center">
                <p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para</p>
                <a href="#" class="botao-primario">Cadastrar</a>
                <a href="#" class="botao-secundario">Entrar</a>
            </div>
        </div>
    </div>
</body>
</html>